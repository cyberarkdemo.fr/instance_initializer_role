#	Author: Jérôme Coste <contact@jeromecoste.fr>
#	Date: 01/09/18
###################
# PS1 declaration #
###################

COLOR=${COLOR:="yes"}
if [ "$USER" = "root" ]
then
	sign="#"
else
	sign="$"
fi

if [ "$COLOR" = "yes" ]
then
	PS1='\[\033[01;31m\]⤷\[\033[00m\] \[\033[01;33m\]\u@\h\[\033[00m\] \[\033[01;32m\]{ \w }\[\033[00m\] \[\033[01;31m\]'$sign'\[\033[00m\] '
	alias ls='ls --color=auto'
else
	PS1='⤷ \u@\h { \w } $ '
fi