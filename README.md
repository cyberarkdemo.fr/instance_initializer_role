# Instance Initializer Role

Configure new instances.

## Features
- [x] vimrc
- [x] PS1 (bash)
- [x] Timezone

## Parameters
 - entity_name: Hostname of instance